
#ifndef DISTANCEREADER_FILE
#define DISTANCEREADER_FILE

typedef struct {
  int l;
  int c;
  int r;
} tripleData;

tripleData getSharpSensorData();

#endif
