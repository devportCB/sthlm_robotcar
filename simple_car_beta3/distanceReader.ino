#include "distanceReader.h"

#define SENSORPORT_L A0
#define SENSORPORT_C A1
#define SENSORPORT_R A2



tripleData getSharpSensorData(){
  int l = analogRead(SENSORPORT_L);
  int c = analogRead(SENSORPORT_C);
  int r = analogRead(SENSORPORT_R);
  tripleData d;
  d.l = l;
  d.c = c;
  d.r = r;
  return d;
}
